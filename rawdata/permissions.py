# Third party apps
from rest_framework import permissions

# This app
from .utils import rawdata_user_has_valid_subscription


class IsOwnerOrReadOnly(permissions.BasePermission):
    """
    Custom permission to only allow owners of an object to edit it.
    """

    def has_permission(self, request, view, obj=None):
        # Skip the check unless this is an object-level test
        if obj is None:
            return True

        # Read permissions are allowed to any request
        if request.method in permissions.SAFE_METHODS:
            return True

        # Write permissions are only allowed to the owner of the snippet
        return obj.user == request.user


class IsSubscriber(permissions.BasePermission):
    def has_permission(self, request, view, obj=None):
        if request.method in permissions.SAFE_METHODS:
            return True

        return rawdata_user_has_valid_subscription(request.user)
