# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2016-01-07 11:28+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0\n"

#: templates/astrobin_apps_images/snippets/image.html:12
msgid "Error!"
msgstr ""

#: templates/astrobin_apps_images/snippets/image.html:55
msgid "Loading"
msgstr ""

#: templates/astrobin_apps_images/snippets/image.html:94
msgid "(no title)"
msgstr ""

#: templatetags/astrobin_apps_images_tags.py:111
msgid "Data corruption. Please upload this image again. Sorry!"
msgstr ""
